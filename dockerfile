FROM node:10-alpine
WORKDIR /app
COPY package.json /app
COPY package-lock.json /app
COPY .env /app
COPY . /app
RUN apk add --no-cache --virtual .gyp python make g++ \
&& npm install && apk del .gyp
# RUN npm install
RUN npm install -g nodemon
EXPOSE 8081
CMD nodemon index.js

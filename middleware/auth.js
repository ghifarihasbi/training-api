require('dotenv')
const jwt = require('jsonwebtoken')
const { Auth } = require('../models/Auth')
const bcrypt = require('bcrypt')

module.exports = async (req, res, next) => {
    try {
        const token = req.header('x-auth-token')
        const userId = req.header('userId')
        if (!token || !userId) return res.status(401).send('Access denied. No provided token')
        const auth = await Auth.findOne({ userId })
        const validateToken = await bcrypt.compare(token, auth.authHashToken)
        const decoded = jwt.verify(token, process.env.jwtPrivateKey)
        if (!auth) return res.status(401).send('Access denied. No provided token')
        if (!validateToken) return res.status(401).send('Access denied. invalid token')
        if (auth.timeOut < new Date().getTime()) return res.status(401).send('your session is finish, please login again')
        req.headers.principal = auth.principal
        req.user = decoded
        next()
    } catch (error) {
        console.log(error)
        res.status(401).send('Access denied, Invalid token')
    }
}
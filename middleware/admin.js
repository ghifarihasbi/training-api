const { User } = require('../models/User')

module.exports = async (req, res, next) => {
    try {
        let user = await User.findOne({ admin: true, email: req.headers.principal.email })
        if (!user) return res.send('only admin cannot delete')
        next()
    } catch (error) {
        res.status(500).send('internal.server.error')
    }
}
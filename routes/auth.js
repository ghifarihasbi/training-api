const Joi = require('joi')
const express = require('express')
const route = express.Router()
const asyncMiddleware = require('../middleware/asycn')
const auth = require('../middleware/auth')
const bcrypt = require('bcrypt')
const { v4: uuidv4 } = require('uuid')
const { User } = require('../models/User')
const { Auth } = require('../models/Auth')

const validateAuth = (req) => {
    const schema = {
        email: Joi.string().min(5).max(255).required().email(),
        password: Joi.string().min(5).max(1024).required()
    }
    return Joi.validate(req, schema)
}

route.post('/login', asyncMiddleware(async (req, res) => {
    const { error } = validateAuth(req.body)
    if (error) return res.status(400).send(error.details[0].message)
    let userAuth = await User.findOne({ email: req.body.email })
    if (!userAuth) return res.status(400).send('email is not registered')
    const validatePassword = await bcrypt.compare(req.body.password, userAuth.password)
    if (!validatePassword) return res.status(400).send('password is invalid')
    let auth = await Auth.findOne({ 'principal.email': req.body.email })
    const token = userAuth.generateAuthToken()
    const salt = await bcrypt.genSalt(10)
    const authHashToken = await bcrypt.hash(token, salt)
    const userId = uuidv4()
    const timeOut = new Date().getTime() + 30 * 60 * 1000
    if (auth) {
        await Auth.deleteOne({ 'principal.email': req.body.email })
    }
    auth = new Auth({ timeOut, userId, authHashToken, principal: { email: req.body.email } })
    await auth.save()
    res.header('userId', userId)
    res.header('x-auth-token', token).send({})
}))
route.post('/logout', auth, asyncMiddleware(async (req, res) => {
    console.log('principal', req.headers.principal)
    await Auth.deleteOne({ sessionId: req.headers.sessionid })
    res.send({})
}))
module.exports = route
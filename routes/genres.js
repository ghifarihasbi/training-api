const express = require('express')
const route = express.Router()
const auth = require('../middleware/auth')
const admin = require('../middleware/admin')
const asycnMiddleware = require('../middleware/asycn')
const { Genre, validateGenre } = require('../models/Genre')


route.get('/', auth, asycnMiddleware(async (req, res) => {
    const genres = await Genre.find().sort('name')
    res.send(genres)
}))

route.post('/', auth, asycnMiddleware(async (req, res) => {
    const { error } = validateGenre(req.body)
    if (error) return res.status(400).send(error.details[0].message)
    let genre = new Genre({ name: req.body.name })
    await genre.save()
    res.send(genre)
}))

route.get('/:id', auth, asycnMiddleware(async (req, res) => {
    console.log('req', req.params)
    let genre = await Genre.findById(req.params.id).exec()
    if (!genre) return res.send('genre not found')
    res.send(genre)
}))

route.put('/:id', auth, asycnMiddleware(async (req, res) => {
    const { error } = validateGenre(req.body)
    if (error) return res.status(400).send(error.details[0].message)
    let genre = await Genre.findByIdAndUpdate(req.params.id, {
        name: req.body.name
    })
    if (!genre) return res.status(401).send('genre not found')
    res.send(genre)
}))

route.delete('/:id', auth, admin, asycnMiddleware(async (req, res) => {
    let genre = await Genre.findByIdAndDelete(req.params.id).exec()
    if (!genre) return res.send('genre not found')
    res.send(genre)
}))

module.exports = route
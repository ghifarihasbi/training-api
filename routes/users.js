const { User, validate } = require('../models/User')
const express = require('express')
const asyncMiddleware = require('../middleware/asycn')
const route = express.Router()
const bcrypt = require('bcrypt')

route.post('/', asyncMiddleware(async (req, res) => {
    const { error } = validate(req.body)
    if (error) return res.status(400).send(error.details[0].message)
    let user = await User.findOne({ email: req.body.email })
    if (user) return res.status(400).send('User is already register')

    user = new User({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password
    })
    const salt = await bcrypt.genSalt(10)
    user.password = await bcrypt.hash(user.password, salt)
    await user.save()
    const token = user.generateAuthToken()
    res.header('x-auth-token', token).send({})
}))

module.exports = route
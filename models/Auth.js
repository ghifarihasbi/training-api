const mongoose = require('mongoose')

const authScema = new mongoose.Schema({
    authHashToken: {
        type: String,
        minlength: 5,
        maxlength: 1024,
        required: true
    },
    principal: {
        type: Object,
        required: true
    },
    userId:{
        type: String,
        minlength: 5,
        maxlength: 1024,
        required: true
    },
    timeOut:{
        type:Number,
        required: true
    }
})

const Auth = mongoose.model('Auth', authScema, 'auth')

exports.Auth = Auth
const mongoose = require('mongoose')
const Joi = require('joi')

const genreSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        minlength: 3,
        maxlength: 25
    }
})
const validateGenre = (genre) => {
    const schema = {
        name: Joi.string().min(3).max(25).required()
    }
    return Joi.validate(genre, schema)
}
const Genre = mongoose.model('Genre', genreSchema, 'genre')
exports.Genre = Genre
exports.validateGenre = validateGenre
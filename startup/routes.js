const error = require('../middleware/error')
const users = require('../routes/users')
const auth = require('../routes/auth')
const genre = require('../routes/genres')
const express = require('express')
module.exports = (app) => {
    app.use(express.json())
    app.use(express.urlencoded({ extended: false }))
    app.use('/api/users', users)
    app.use('/api/auth', auth)
    app.use('/api/genre', genre)
    app.use(error)
}
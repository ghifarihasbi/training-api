const winston = require('winston')
require('winston-mongodb')

module.exports = () => {
    winston.exceptions.handle(
        new winston.transports.Console(),
        new winston.transports.File({ filename: 'exceptions.log' }),
    );
    process.on('unhandledRejection', (err) => {
        throw err
    })
    winston.add(new winston.transports.Console(),new winston.transports.File({ filename: 'logfile.log' }))
    winston.add(new winston.transports.MongoDB({
        db: 'mongodb://192.168.80.3/Training',
        level: 'info',
        options: {
            useUnifiedTopology: true,
            useNewUrlParser: true,
        }
    }))
}
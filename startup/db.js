const winston = require('winston')
const mongoose = require('mongoose')
module.exports = (dbConnect) => {
    const option = {
        useUnifiedTopology: true,
        useNewUrlParser: true,
        useCreateIndex: true,
        useFindAndModify:false
    }
    mongoose.connect(dbConnect, option)
        .then(() => winston.info('CONNECT TO ' + dbConnect))
}
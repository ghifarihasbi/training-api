require('dotenv').config()
const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)
const express = require('express')
const winston = require('winston')
const app = express()
const port = process.env.PORT
const node_env = process.env.NODE_ENV
const dbConnect = node_env === 'development' ? process.env.DBCONNECTDEV : process.env.DBCONNECTPROD
require('./startup/logging')()
require('./startup/routes')(app)
require('./startup/db')(dbConnect)

winston.info('STAGE ' +  node_env)

app.listen(port, () => {
    winston.info(`listening port ${port}...`)
})